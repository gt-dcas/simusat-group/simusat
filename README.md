# Simusat 


## Simusat v6.4.3 - Standalone (placer le zip sous D:/ et décompresser, pas d'installation requise)

[Simusat v6.4.3](https://gitlab.isae-supaero.fr/gt-dcas/simusat-group/simusat-dev/-/package_files/37/download)


### A l'ISAE-supaero

Install in D:/ 

## Simusat (old version) VB6 download .exe and install

[Software - Simusat STABLE- VB6 V03](https://sourceforge.isae.fr/projects/miniproject_intro_space_systems/repository/revisions/master/raw/softs/oldVersions/Simusat_Vb6_V03_setup.exe)

(windows version)




## Troubleshooting

### EN

In some windows version, there is an incopatibility issue with the decimal formatting - franch "," versus US ".":
to change it, change system parameters (regional parameters , data format,, date format, numerical format, choose "." for Simusat)

### FR
Panneau de configuration\Horloge et région
Modifier les formats de date, d'heure ou de nombre
Paramètres supplémentaires
Symbole decimal : '.'


